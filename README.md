#Finance App

This is PWA I've been using to track finances - it's focused on mobile platform usage.

Use firebase as backend and publish platform

####Getting started:
* To build and watch:
```bash
npm install
npm start
```
* To raise local server:
```bash
npm run local-server
```

![screen](README/finance1.jpeg)
![screen](README/finance2.jpeg)
